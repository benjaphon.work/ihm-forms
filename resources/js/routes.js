import Form from './components/FormComponent.vue'
import Leads from './views/Leads.vue'
import LeadMetas from './views/LeadMetas.vue'
 
export const routes = [
    {
        name: 'form',
        path: '/',
        component: Form
    },
    {
        name: 'leads',
        path: '/leads',
        component: Leads
    },
    {
        name: 'lead_metas',
        path: '/lead_metas/:leadId',
        component: LeadMetas
    },
];
<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Lead;
use App\Models\LeadMeta;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $lead = [];

        $lead['leadId'] = $request['leadId'];

        //All Steps
        // foreach ($request->all() as $step) {
        //     foreach ($step['inputs'] as $input) {
        //         if (isset($input['value'])) {
        //             $leadData[$input['name']] = $input['value'];
        //         }
        //     }
        // }

        //Only Current Step
        foreach ($request['leadData']['inputs'] as $input) {
            if (isset($input['value'])) {
                $lead['leadData'][$input['name']] = $input['value'];
            }
        }

        // dd($lead);

        if ($modelLead = Lead::firstOrCreate(['uuid' => $lead['leadId']])) {
            if (isset($lead['leadData'])) {
                foreach ($lead['leadData'] as $field => $value) {
                    LeadMeta::updateOrCreate([
                        'lead_id' => $modelLead->id,
                        'field' => $field,
                    ], [
                        'value' => $value,
                    ]);
                }
            }
        }

        return dd($lead);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $options = [
            [
                'title' => "Get A quote Today - No Obligation!",
                'subtitle' => "Easy Application. Only Takes 60 Seconds",
                'inputs' => [
                    [
                        'type' => 'continue',
                        'label' => 'Get Started',
                    ],
                ],
            ],
            [
                'title' => "What's the nature of your project?",
                'inputs' => [
                    [
                        'type' => 'option',
                        'name' => 'nature',
                        'lists' => [
                            [
                                'label' => 'Repair Windows',
                                'value' => 'repair',
                            ],
                            [
                                'label' => 'Replace Windows',
                                'value' => 'replace',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'title' => "How many windows does your project involve?",
                'inputs' => [
                    [
                        'type' => 'option',
                        'name' => 'windows',
                        'lists' => [
                            [
                                'label' => '1',
                                'value' => '1',
                            ],
                            [
                                'label' => '2',
                                'value' => '2',
                            ],
                            [
                                'label' => '3-5',
                                'value' => '3_5',
                            ],
                            [
                                'label' => '6-9',
                                'value' => '6_9',
                            ],
                            [
                                'label' => '10',
                                'value' => '10',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'title' => "What is your name?",
                'inputs' => [
                    [
                        'type' => 'text',
                        'name' => 'first_name',
                        'label' => 'First Name',
                        'pattern' => '^([a-zA-Z- ]+)$',
                        'placeholder' => 'First Name',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'text',
                        'name' => 'last_name',
                        'label' => 'Last Name',
                        'pattern' => '^([a-zA-Z- ]+)$',
                        'placeholder' => 'Last Name',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'continue',
                    ],
                ],
            ],
            [
                'title' => "Where will this project take place?",
                'inputs' => [
                    [
                        'type' => 'text',
                        'name' => 'address',
                        'label' => 'Address',
                        'placeholder' => '123 Your St.',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'text',
                        'name' => 'post_code',
                        'label' => 'Postcode',
                        'pattern' => '^([0-9]{5})$',
                        'placeholder' => 'For example: 91210',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'radio',
                        'label' => 'Purchase Time Frame',
                        'name' => 'purchase_time_frame',
                        'value' => 'immediately',
                        'lists' => [
                            [
                                'label' => 'Immediately',
                                'value' => 'immediately',
                            ],
                            [
                                'label' => 'Within 1 months',
                                'value' => 'within_1_month',
                            ],
                            [
                                'label' => '1-3 months',
                                'value' => '1_3_months',
                            ],
                            [
                                'label' => '3+ months',
                                'value' => '3_months',
                            ],
                        ],
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'checkbox',
                        'name' => 'is_owner',
                        'value' => true,
                        'label' => 'I am authorized to make changes to the property',
                    ],
                    [
                        'type' => 'continue',
                    ],
                ],
            ],
            [
                'title' => 'Please enter your phone number and email address',
                'inputs' => [
                    [
                        'type' => 'text',
                        'name' => 'phone',
                        'label' => 'Please enter a Valid mobile Number',
                        'pattern' => '^([2-9][0-9]{9})$',
                        'placeholder' => '(999) 999-9999',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'radio',
                        'label' => 'Best time to call',
                        'name' => 'best_time_to_call',
                        'value' => 'morning',
                        'lists' => [
                            [
                                'label' => 'Morning',
                                'value' => 'morning',
                            ],
                            [
                                'label' => 'Afternoon',
                                'value' => 'afternoon',
                            ],
                            [
                                'label' => 'Evening',
                                'value' => 'evening',
                            ],
                        ],
                    ],
                    [
                        'type' => 'text',
                        'name' => 'email',
                        'label' => 'Email Address',
                        'pattern' => '^([A-Za-z0-9_\-.+])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,})$',
                        'placeholder' => 'Email Address',
                        'required' => true,
                        'invalid' => false,
                    ],
                    [
                        'type' => 'continue',
                        'label' => 'Get a quote today - no obligation!',
                    ],
                    [
                        'type' => 'accept',
                        'text' => "We respect your data and privacy. By clicking
                        <strong>Get a quote today - no obligation!</strong>,
                        you agree to the
                        <a
                            href='/windows/docs/legal/terms-service'
                            class='legal-link'
                            >Terms of Service</a
                        >
                        and
                        <a
                            href='/windows/docs/legal/privacy-policy'
                            class='legal-link'
                            >Privacy Policy</a
                        >
                        and you authorize Home-improvements.co, its
                        <a
                            href='/windows/docs/legal/partners'
                            class='legal-link'
                            >partners</a
                        >
                        and up to four home improvement companies to call
                        the phone number provided, using automated phone
                        technology and text messaging to contact you. By
                        giving your authorization you are not obligated to
                        purchase products or services, and you understand
                        that you may revoke your consent at any time. Mobile
                        and data charges may apply. For Californian
                        residents, please refer to our
                        <a
                            href='/windows/docs/legal/ccpa-privacy-policy'
                            class='legal-link'
                            >CCPA privacy</a
                        >.",
                    ],
                ],
            ],
            [
                'title' => 'Thank you for your application!',
                'inputs' => [
                    [
                        'type' => 'thank-you',
                        'text' => "<h3>
                        Keep an eye out for NEW phone numbers! It's
                        probably us trying to contact you.
                    </h3>
                    <br />
                    <p>
                        <strong>1. One of our local pro’s</strong
                        ><br />will be in touch with you very soon!
                    </p>
                    <p>
                        IMPORTANT: Whilst we aim to call you back as
                        soon as possible, we know that this is important
                        to you and if you are only available to discuss
                        right now, please call on
                        <strong
                            ><a href='tel:(888) 219-4911'
                                >(888) 219-4911</a
                            ></strong
                        >
                    </p>
                    <p>
                        <strong>2. Please make sure</strong><br />you
                        have your phone on you at all times.
                    </p>
                    <p>
                        <strong>3. During the call,</strong><br />make
                        sure our Pro’s have accurately confirmed your
                        request.
                    </p>
                    <p>
                        <strong>4. Feel free to take your time</strong
                        ><br />and ask as many questions as you like.
                        We’re here to serve you!
                    </p>",
                    ],
                ],
            ],
        ];

        return response()->json($options);

        // $form = Form::find($id);
        // return response()->json($form);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\LeadMeta;
use Illuminate\Http\Request;

class LeadMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeadMeta  $leadMeta
     * @return \Illuminate\Http\Response
     */
    public function show(LeadMeta $leadMeta)
    {
        return $leadMeta->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeadMeta  $leadMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(LeadMeta $leadMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeadMeta  $leadMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeadMeta $leadMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeadMeta  $leadMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeadMeta $leadMeta)
    {
        //
    }
}

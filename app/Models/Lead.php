<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Lead extends Model
{
    use HasFactory;
    protected $fillable = ['uuid'];
    protected $appends = ['lead_metas'];

    /**
     * Get all of the lead_metas for the Lead
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lead_metas()
    {
        return $this->hasMany(LeadMeta::class);
    }

    public function getLeadMetasAttribute()
    {
        return $this->lead_metas()->get();
    }
}

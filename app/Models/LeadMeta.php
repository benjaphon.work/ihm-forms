<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadMeta extends Model
{
    use HasFactory;
    protected $fillable = ['lead_id', 'field', 'value'];
    public $timestamps = false;

    /**
     * Get the lead that owns the LeadMeta
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lead(): BelongsTo
    {
        return $this->belongsTo(Lead::class);
    }
}
